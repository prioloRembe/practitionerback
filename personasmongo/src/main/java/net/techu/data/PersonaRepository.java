package net.techu.data;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonaRepository extends MongoRepository<PersonaModel, String> {

    @Query("{'nombre':?0}")
    public List<PersonaModel> findByNombre(String nombre);

    /*@Query("{'precio': {$gt: ?0, $lt: ?1}}")
    public List<Persona> findByPrecio(double minimo, double maximo);
*/
}
