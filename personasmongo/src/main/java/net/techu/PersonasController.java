package net.techu;

import net.techu.data.PersonaModel;
import net.techu.data.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PersonasController {

    @Autowired
    private PersonaRepository repository;

    @GetMapping("/personas")
    public ResponseEntity<List<PersonaModel>> getPersonas() {
        List<PersonaModel> lista = repository.findAll();
        return new ResponseEntity<List<PersonaModel>>(lista, HttpStatus.OK);
    }

    @GetMapping("/personas/{id}")
    public ResponseEntity<String> getPersonasPorId(@PathVariable String id) {
        Optional<PersonaModel> persona = repository.findById(id);
        if (persona.isPresent()) {
            return new ResponseEntity<String>(String.valueOf(persona), HttpStatus.OK);
        }
        return new ResponseEntity<>("Persona no encontrada", HttpStatus.NOT_FOUND);
    }

    @PostMapping("/personas")
    public ResponseEntity<PersonaModel> addPersonas(@RequestBody PersonaModel personaModel) {
        PersonaModel persona = repository.save(personaModel);
        return new ResponseEntity<PersonaModel>(persona, HttpStatus.OK);
    }

    @PutMapping(value = "/personas/{id}")
    public ResponseEntity<String> ModifyPersona(@PathVariable String id, @RequestBody PersonaModel personaModel) {
        Optional<PersonaModel> persona = repository.findById(id);
        if (persona.isPresent()) {
            repository.save(personaModel);
            return new ResponseEntity<String>("Persona Modificada.", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<String>("Persona no encontrada.", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/personas/{id}")
    public ResponseEntity<String> deletePersona(@PathVariable String id) {
        Optional<PersonaModel> persona = repository.findById(id);
        if (persona.isPresent()) {
            repository.deleteById(id);
            return new ResponseEntity<String>("Persona eliminada.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Persona no encontrada", HttpStatus.NOT_FOUND);
    }

    @PatchMapping(value = "/personas/{id}")
    public ResponseEntity<String> updatePersona(@PathVariable String id, @RequestBody PersonaModel personaModel) {
        Optional<PersonaModel> persona = repository.findById(id);
        if (persona.isPresent()) {
            PersonaModel personaNew = persona.get();
            personaNew.salario = personaModel.salario;
            personaNew.activo = personaModel.activo;
            PersonaModel guardado = repository.save(persona.get());
            return new ResponseEntity<String>("Persona actualizada.", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<String>("Persona no encontrada", HttpStatus.NOT_FOUND);
    }

}
