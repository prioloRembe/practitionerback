package net.techu;

import net.techu.data.Persona;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PersonasController {

    private ArrayList<Persona> listaPersonas = null;

    public PersonasController() {
        listaPersonas = new ArrayList<>();
        listaPersonas.add(new Persona(1, "Juan", "Moreno", 1200000, true));
        listaPersonas.add(new Persona(2, "Pedro", "perez", 3300000, false));
        listaPersonas.add(new Persona(3, "Maria", "Trujillo", 5804000, true));
    }

    @GetMapping("/personas")
    public ResponseEntity<List<Persona>> obtenerPersonas() {
        return new ResponseEntity<List<Persona>>(listaPersonas, HttpStatus.OK);
    }

    @GetMapping("/personas/{id}")
    public ResponseEntity obtenerPersonasPorId(@PathVariable int id) {
        try {
            Persona persona = listaPersonas.get(id - 1);
            return ResponseEntity.ok(persona);
        } catch (Exception ex) {
            return new ResponseEntity<>("Persona no encontrada.", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/personas")
    public ResponseEntity<String> crearPersona(@RequestBody Persona persona) {
        listaPersonas.add(persona);
        return new ResponseEntity<>("Persona creada.", HttpStatus.CREATED);
    }

    @PutMapping("/personas/{id}")
    public ResponseEntity ModifyPersona(@PathVariable int id, @RequestBody Persona persona) {
        try {
            Persona personaModificada = listaPersonas.get(id - 1);
            listaPersonas.set(id - 1, persona);
            return new ResponseEntity<>("Persona actualizada.", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("Persona no encontrada", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/personas/{id}")
    public ResponseEntity deletePersonas(@PathVariable int id) {
        try {
            Persona persona = listaPersonas.get(id - 1);
            listaPersonas.remove(id - 1);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>("Persona no encontrada", HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/personas/{id}")
    public ResponseEntity<String> updatePersona(@PathVariable int id, @RequestBody Persona persona) {
        try {
            Persona personaModificada = listaPersonas.get(id - 1);
            personaModificada.setSalario(persona.salario);
            personaModificada.setActivo(persona.activo);
            listaPersonas.set(id - 1, personaModificada);
            return new ResponseEntity<>("Persona Modificada.", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>("Persona no encontrada", HttpStatus.NOT_FOUND);
        }
    }

}
